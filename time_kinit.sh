#!/bin/bash

# Iterations are set per test, so more time-consuming tests can be run less
# often
ITERATIONS=100

source `dirname $0`/utils.sh

set_up () {
	set_krb_env
	setup_kinit
}

tear_down () {
	restore_krb_env
}

set_up

PRINCIPAL=$( get_principal $1)
PASSWORD=$( get_password $1)

echo -e "\tKINIT ${PRINCIPAL}"

START_TIME=$( start_timer )

echo -en "\t"
for i in $(${SEQ} 1 $ITERATIONS); do
	call_kinit "${PRINCIPAL}" "${PASSWORD}"
	${KDESTROY}
	echo -n "."
done
echo "done"

STOP_TIME=$( stop_timer )

TOTAL_TIME=$( total_time $START_TIME $STOP_TIME )

echo -e "\t\ttotal time:\t\t${TOTAL_TIME}s"

LOGINS_PER_MINUTE=$(iterations_per_minute $START_TIME $STOP_TIME $ITERATIONS)

echo -e "\t\titerations/min:\t\t$LOGINS_PER_MINUTE"

tear_down

