#!/bin/bash

DATE=date
BC=bc
SED=sed
DATE_FORMATSTR="+%s.%N"

KINIT=kinit
# MIT krb < 1.6
KINIT_PARAM_OLD="--password-file=STDIN"
# MIT krb >= 1.6
KINIT_PARAM_NEW=""

KDESTROY=kdestroy
SEQ=seq

NEW_KRB5CCNAME=/tmp/ad_test_ccname

NET=/home/kai/samba/s3/bin/net
CONFIG_FILE=`dirname $0`/smb.conf

RUNS=`dirname $0`/runs.txt
