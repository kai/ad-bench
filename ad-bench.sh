#!/bin/bash

source `dirname $0`/utils.sh

if [ ! -f $RUNS ]; then
	echo "Error: please fill in $RUNS"
	echo "Sambple entries are"
	echo "user@REALM.EXAMPLE.COM%password:domain_controller"
	exit 1
fi

for run in `cat $RUNS`; do
	echo "START RUN"
	bash `dirname $0`/time_kinit.sh `echo $run|cut -d ":" -f 1`
	bash `dirname $0`/time_join.sh `echo $run|cut -d ":" -f 1` `echo $run|cut -d ":" -f 2`
	bash `dirname $0`/time_user.sh `echo $run|cut -d ":" -f 1` `echo $run|cut -d ":" -f 2`
	bash `dirname $0`/time_group.sh `echo $run|cut -d ":" -f 1` `echo $run|cut -d ":" -f 2`
	bash `dirname $0`/time_ldap.sh `echo $run|cut -d ":" -f 1` `echo $run|cut -d ":" -f 2`
	echo "END RUN"
done

